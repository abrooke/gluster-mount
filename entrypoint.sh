#!/bin/bash

set -e
                                                                                                   
function unmount_gluster {                                                                         
  echo "trapped signal $*"                                                                         
  
  echo "send SIGTERM to mount pid $MOUNT_PID"                                                      
  kill -TERM $MOUNT_PID                                                                            
  
  echo "unmount /gluster/$MOUNT_DIR"                                                               
  umount /gluster/$MOUNT_DIR
  
  echo "remove directory /gluster/$MOUNT_DIR"                                                      
  rmdir /gluster/$MOUNT_DIR
}                                                                                                  
                                                                                                   
# Unmount the Gluster volume when we receive a signal from Docker                                  
# http://veithen.github.io/2014/11/16/sigterm-propagation.html                                     
trap unmount_gluster INT TERM                                                                      
                                                                                                   
mkdir -p /gluster/$MOUNT_DIR

"$@" &                                                                                             
MOUNT_PID=$!                                                                                       
                                                                                                   
echo "wait for pid $MOUNT_PID"                                                                     
wait $MOUNT_PID                                                                                    
