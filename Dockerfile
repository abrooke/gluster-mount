FROM buildpack-deps:jessie-curl

ARG GLUSTER_VERSION

RUN \
  apt-get update \
  && apt-get install -y --no-install-recommends apt-transport-https \
  && wget -O - https://download.gluster.org/pub/gluster/glusterfs/$GLUSTER_VERSION/LATEST/rsa.pub | apt-key add - \
  && echo "deb http://download.gluster.org/pub/gluster/glusterfs/$GLUSTER_VERSION/LATEST/Debian/jessie/apt jessie main" > /etc/apt/sources.list.d/gluster.list \
  && apt-get update \
  && apt-get install -y --no-install-recommends glusterfs-client \
	&& rm -rf /var/lib/apt/lists/* \
	&& sed -i -e 's/\(.*\)echo "\$cmd_line \$mount_point"/\1echo "\$cmd_line --no-daemon \$mount_point"/g' /sbin/mount.glusterfs

ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/bin/bash", "/entrypoint.sh" ]

CMD mount -t glusterfs -o ${MOUNT_OPTS:-defaults},log-file=/dev/stdout $GLUSTER_VOLUME /gluster/$MOUNT_DIR
