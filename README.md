# gluster-mount

A simple Docker image for glusterfs-client to mount a Gluster volume for your Docker containers.

## Available versions

See all images on the [registry](https://gitlab.com/abrooke/gluster-mount/container_registry).

## Usage

* Prepare a Gluster volume on your Gluster cluster.

```
root@s1:~# gluster volume create gluster1 replica 2 s1:/export/gluster1 s2:/export/gluster1
```

* Run the gluster-mount container to mount the Gluster volume on the Docker host.

Example command for a volume `gluster1` hosted on `172.19.0.1`. The volume will be accessible on the Docker host at `/gluster/gluster1`. 

```
[rancher@rancher ~]$ docker run -d --add-host s1:172.19.0.1 --add-host s2:10.20.30.40 --privileged -v /gluster:/gluster:shared -e GLUSTER_VOLUME=172.19.0.1:gluster1 -e MOUNT_DIR=gluster1 registry.gitlab.com/abrooke/gluster-mount:3.10
```

You may need the `--add-host` parameters if the bricks are reachable at a different address that the address of the bricks in the Gluster cluster (e.g. your Docker instance runs in a VM, and `s1` resolves to an unreachable IP from the VM).

* Run the containers you want and bind their data volumes to the gluster volume on the Docker host

```
[rancher@rancher ~]$ docker run -d --name mypostgres -v /gluster/gluster1/mypostgres:/var/lib/postgresql/data postgres
```

* You can test on *another host* of your Gluster cluster that data is here (the `gluster1` volume is supposed to me mounted at `/mnt/gluster1/`)

```
root@s2:~# ls -alh /mnt/gluster1/mypostgres/
total 107K
drwx------ 19  999 root   4,0K avril 11 07:39 .
drwxr-xr-x  6 root root   4,0K avril 11 07:37 ..
drwx------  5  999 docker 4,0K avril 11 07:38 base
drwx------  2  999 docker 4,0K avril 11 07:40 global
drwx------  2  999 docker 4,0K avril 11 07:38 pg_clog
drwx------  2  999 docker 4,0K avril 11 07:38 pg_commit_ts
drwx------  2  999 docker 4,0K avril 11 07:38 pg_dynshmem
-rw-------  1  999 docker 4,4K avril 11 07:38 pg_hba.conf
-rw-------  1  999 docker 1,6K avril 11 07:38 pg_ident.conf
...
```

## Known issues

* The container is run with `--privileged`.
